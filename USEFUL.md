
# Key generator for android-certs

```
export subject='/C=US/ST=California/L=Mountain View/O=Android/OU=Android/CN=Android/emailAddress=android@android.com'
[ ! -d "${HOME}/.android-certs" ] && mkdir -p ~/.android-certs
for x in releasekey platform shared media networkstack network; do \
    ./development/tools/make_key ~/.android-certs/$x "$subject"; \
done
```
