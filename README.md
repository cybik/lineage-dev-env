# LineageOS base build environment

For the lazy blokes like me that don't want to change their local environment to build Lineage.

# Building the container locally

Since I'm not pushing this image to hub.docker.com (yet), yall have to build it locally.

```
docker build .
```

# Tagging the container

This is basically the nerd equivalent of a User Experience improvement - associating a shortname with a docker image hash.

```
docker tag <docker hash> <wanted tag>
```

# Running the container

## Run container directly

This command will shut the container down if you exit the session that launched the container.
If you're going to launch alternate shells using `docker exec`, skip this and hit the next tip.

```
docker run --volume <host directory to use>:/lineagebuild -it <wanted tag> /bin/bash
```

Personal advice? Run it in the background.

# Run container in the background
This command will launch the container in the background. You have to use a `docker exec` command to access it.

```
docker run --volume <host directory to use>:/lineagebuild -it -d <wanted tag>
# the above command will return a hash. REUSE IT IN THE NEXT COMMAND.
docker exec -it <the good hash above> bash
```

# Initialize

## Using the normal tools
Basically, [this](https://github.com/LineageOS/android#getting-started).
```
repo init -u git://github.com/LineageOS/android.git -b lineage-17.1
repo sync
```

## Using Easyinit

I wrote a [small shell script](https://gitlab.com/cybik/lineage-dev-env/blob/master/scripts/easyinit) to speed up the process.
```
easyinit -u "Your Name" -e "Your email" -b <Lineage branch to repoinit>
```

## Using Easyinit - Autosync variant!

Did I mention I'm lazy?
```
easyinit -u "Your Name" -e "Your email" -b <Lineage branch to repoinit> -s
```
