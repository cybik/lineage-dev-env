FROM ubuntu:focal

# Baseline tools
ENV DEBIAN_FRONTEND="noninteractive"
# TZ="America/Los_Angeles"
RUN apt update && apt -y install tzdata && ln -fs /usr/share/zoneinfo/America/Los_Angeles /etc/localtime && dpkg-reconfigure --frontend noninteractive tzdata
RUN apt update && apt -y install bc bison build-essential ccache curl flex 
RUN apt update && apt -y install g++-multilib gcc-multilib git gnupg gperf imagemagick
RUN apt update && apt -y install lib32ncurses5-dev lib32readline-dev lib32z1-dev liblz4-tool libncurses5-dev libsdl1.2-dev libssl-dev
RUN apt update && apt -y install python-wxgtk3.0-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools xsltproc zip
RUN apt update && apt -y install zlib1g-dev vim nano python3.8 python3.8-venv python3-virtualenv wget git-lfs
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
#RUN apt update && apt -y install openjdk-9-jdk-headless

# Repo
ENV PATH="/usr/local/bin:${PATH}"
RUN wget -O /usr/local/bin/repo https://storage.googleapis.com/git-repo-downloads/repo && chmod a+x /usr/local/bin/repo

# Workspaceing
# ATTENTION: with a Compose, this'll automount to local. Without, you'll need to manually mount a local dir when you
#            launch the image. Please keep this in mind
RUN mkdir /lineagebuild
VOLUME /lineagebuild

RUN apt update && apt -y install libncurses5 libncurses5-dev

# Launch Env, basically
COPY scripts/easyinit /usr/local/bin/easyinit
RUN chmod 744 /usr/local/bin/easyinit
WORKDIR /lineagebuild

RUN apt install -y zsh && sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)" --unattended && chsh -s /bin/zsh

RUN apt install -y fonts-powerline locales && locale-gen en_US.UTF-8

ENV HISTFILE=/root/.zsh_hist/zsh_history \
    TERM=xterm

CMD [ "/bin/zsh" ]
